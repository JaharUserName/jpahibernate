package com.akbarow.tutorialsjpahibernate;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Simple Oracle Read and Write
 * */
public class App 
{
    public static void main( String[] args ) throws Exception
    {
    	Class.forName("oracle.jdbc.driver.OracleDriver");
    	Connection connection = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl", "developer", "cahar97");
    	
    	Statement statement = connection.createStatement();
    	
    	ResultSet resultSet = statement.executeQuery("CREATE TABLE CUSTOMERS(CUSTOMER_ID NUMBER(3), CUSTOMER_NAME VARCHAR(2))");
    	
        System.out.println( "Just done" );
        
    }
}
